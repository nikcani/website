{
  "title": "imprint/privacy",
  "slug": "imprint-privacy"
}

# Impressum

Niklas Wildenburg  
Gelpestraße 96  
51647 Gummersbach  
Telefon [+49 1578 9136622](tel:004915789136622)  
E-Mail [contact@nikcani.de](mailto:contact@nikcani.de)

**Vertretungsberechtigter**  
Niklas Wildenburg

**Verantwortlicher i.S.d. Presserechts**  
Niklas Wildenburg

# Datenschutz

Diese Seite speichert und verarbeitet **keine** personenbezogenen Daten.
Diese Website wird auf Servern von [GitLab](https://gitlab.com/) gehostet. Beachten Sie daher auch die [GitLab Privacy Policy](https://about.gitlab.com/privacy/).
