{
  "title": "curriculum vitae",
  "slug": "cv"
}

# work experience

11/2022 - today  
**Software Developer**  
GRAW Radiosondes (Nuremberg)  

01/2016 - 10/2022  
**Web Developer**  
Kiwis & Brownies (Gummersbach)  

01/2017 - 12/2021  
**Web Designer & Partner**  
fention GbR (Gummersbach)  

# education

09/2015 - 01/2023  
**Bachelor of Science (grade 1.8) - Media Information Technology**  
TH Köln - University of Applied Sciences  
*Technische Hochschule Köln (Campus Gummersbach)*  

09/2007 - 06/2015  
**Abitur (grade 2.1)**  
Städt. Lindengymnasium Gummersbach  
*(formerly: Gymnasium Grotenbach Gummersbach)*  

# international experience

06/2015 - 08/2015  
**Stay abroad**  
Saskatchewan & Vancouver, Canada  

# social engagement

2007 - today  
**Volunteer Fire Brigade Gummersbach**  

09/2013 - 06/2014  
**Certified school mediator**  

# skills

## Languages  
German: native  
English: fluent in speaking and writing B2/(C1)  

## Dev  
**Mostly**  
Object-oriented programming in PHP & JavaScript  
Web Backend with Laravel & Symfony  
Frameworks such as Laravel Nova & filament  
Web Frontend in Vue.js, semantic HTML, CSS & ECMAScript  
Static Site Generators like Hugo  
Wordpress CMS  

**Sometimes**  
C, Java/Kotlin, C#, Python & Rust  
Magento eCommerce  
vTiger CRM  
Building & Deploying Android/iOS Apps  

## Ops
**Mostly**  
Linux Servers (Hetzner Cloud, On Premises, ...)  
Nginx/Apache  
Docker & Kubernetes  

**Sometimes**  
HyperV/VirtualBox  
Microsoft IIS  

## Tooling
Development OS: macOS, Debian based Linux & Windows (WSL2)  
Git & GitLab/GitHub  
JetBrains IDEs
Bash/ZSH & custom shell scripts  

## Graphics, Layout and UI/UX-Design
Expert in Photoshop & Adobe XD  
Proficient in Illustrator & InDesign  

*Last update: June 2023*
